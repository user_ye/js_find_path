class Sorted {
  constructor(data,compare) {
    // 赋值数组
    this.data = data.slice()
    // 排序函数 外边传递 
    this.compare = compare || ((a,b) => a-b)
  }

  // 进行决定往哪个方向走
  take() {
    // 考虑到null 也是可以参与比较的 ，这里返回null是不合适的
    if (!this.data.length) return;
    // 记录最小的值
    // 默认第一个位置是最小值
    let min = this.data[0]
    // 记录最小值的位置
    let minIndex = 0;

    // console.log('long len:' + this.data.length)

    // 开始比较数组里面 所有值 找到更小的值， 就记录为 min
    // 同时记录最小值 ， 和 最小值的位置 这里比较是 点 和 终点的比较 其实就是  x1的平方 + y1的平方     x2的平方 + y2的平方  谁的平方小 谁就是最小值
    for (let i = 0; i < this.data.length;i++) {
      if (this.compare(this.data[i], min) < 0) {
        min = this.data[i];
        minIndex = i;
      }
    }

    // 现在要把最小值拿出去， 移除数据
    // 如果使用splice 则会把所有的位置往前挪动 会有个 O(N) 的时间复杂度
    // 这里技巧：把数组里面的最后值挪动到当前发现最小值的位置 最后使用.pop 最后一位数据移除
    this.data[minIndex] = this.data[this.data.length - 1];
    this.data.pop();
    return min;
  }

  // 塞进 新增 数组
  give(value) {
    this.data.push(value)
  }

  // 获取长度
  get length() {
    return this.data.length;
  }
}