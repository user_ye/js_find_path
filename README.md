## JS 动态演示寻路算法， 原文：https://zhuanlan.zhihu.com/p/273441782

### 最好是去看原文

### 按住鼠标左键滑动即可变成墙体

### 按住鼠标右键滑动即可消除墙体

### 代码说明：

- `main.js`
  - 参数：`tFlag` `true 或 false` 是否进行阻塞渲染 可达到动态效果
  - 参数：`tRate` 阻塞延迟的概率 `0-1`
  - 参数：`logRlag` `true 或 false` 是否打印参数

### 演示如下图片：

- ![](./demo.png)

### snake.html 动态演示自动吃食物 以及绘制路线

- 如下情况：不能寻路

* 食物出现在身体包围圈内
* 身体太长 完全隔离开了食物 （甚至可能尾巴也被隔离开）
* 思路：没找到路径 就去找尾巴的路径 需自行扩展
  - 尾巴的路径也没找到 （最长算法 取其中一个极值进行）
